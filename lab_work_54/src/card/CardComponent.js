import React from 'react';

const suits = {
	H: {className: 'hearts', suitIcon: '♥' },
	C: {className: 'clubs', suitIcon: '♣' },
	D: {className: 'diams', suitIcon: '♦' },
	S: {className: 'spades', suitIcon: '♠' },
};

const CardComponent = props => {
	const classes = `card rank-${props.rank.toLowerCase()} ${suits[props.suit].className}`;
	const suitIcon = suits[props.suit].suitIcon;

	return (
		<div className={classes}>
			<span className="rank">{props.rank.toUpperCase()}</span>
			<span className="suit">{suitIcon}</span>
		</div>
	)
};

export default CardComponent;
