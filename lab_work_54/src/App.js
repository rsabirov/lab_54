import React, {Component} from 'react';
import CardComponent from './card/CardComponent'
import WinCalculator from './WinCalculator'

const Card = function(rank, suit) {
	this.rank = rank;
	this.suit = suit;
	this.id = this.generateId();
};

Card.prototype.generateId = function() {
	return this.rank + this.suit.charAt(0);
};

const rank = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'j', 'q', 'k', 'a'];
const suit = ['D', 'S', 'H', 'C'];
const deck = [];

class App extends Component {
	
	state = {
		cards: [],
		outcome: ''
	};

	constructor(props) {
		super(props);
    	this.generateDeck()
	};

  generateDeck() {
    for(let i = 0; i < rank.length; i++) {
      for(let j = 0; j < suit.length; j++) {
        const card = new Card(rank[i], suit[j]);
        deck.push(card);
      }
    }
  };

  getCards = () => {
	deck.sort(function() {
	  return Math.random() - 0.5;
	});

	const cards = [];

	while(cards.length < 5) {
		const card = deck.pop();

		if(card === undefined) {
			break;
		}

		cards.push(card);
	}

      const calc = new WinCalculator(cards);
      const outcome = calc.getBestHand();

      this.setState({cards, outcome});
  };

	render() {

		return (
			<div className="playingCards faceImages">
				<button onClick={this.getCards} className="button">Раздать карты</button>
				<div className="cardsList">
					{
						this.state.cards.map((card) => {
							return <CardComponent suit={card.suit} rank={card.rank} key={card.id} />
						})
					}
				</div>
				<div className="outcome">{this.state.outcome}</div>
			</div>
		);
	}
}

export default App;
